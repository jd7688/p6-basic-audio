/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
	DBG("Intialising");
    setSize (500, 400);
	MainComponent::audioDeviceManager.initialiseWithDefaultDevices(2, 2); //2 inputs, 2 outputs
	MainComponent::audioDeviceManager.addAudioCallback(this);
    
    ampSliderMain.setSliderStyle(Slider::LinearVertical);
    ampSliderMain.setRange(0., 1.);
    addAndMakeVisible(ampSliderMain);
    ampSliderMain.addListener (this);
    
    MainComponent::ampMain = 0;
    DBG("Set");
}

MainComponent::~MainComponent()
{
	MainComponent::audioDeviceManager.removeAudioCallback(this);
}

void MainComponent::resized()
{
    ampSliderMain.setBounds(50, 10, 20, 380);
}

void MainComponent::audioDeviceAboutToStart(AudioIODevice *audioDeviceManager)
{
	DBG("AudioDeviceAboutToStart");
}

void MainComponent::audioDeviceStopped()
{
	DBG("AudioDeviceStopped");
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    ampMain = ampSliderMain.getValue();
}

void MainComponent::audioDeviceIOCallback(const float** inputChannelData,
	int numInputChannels,
	float** outputChannelData,
	int numOutputChannels,
	int numSamples)
{
	DBG("Audio Callback");
	const float *inL = inputChannelData[0];
	const float *inR = inputChannelData[1];
	float *outL = outputChannelData[0];
	float *outR = outputChannelData[1];
	while (numSamples--)
	{
		*outL = *inL * ampMain;
		*outR = *inR * ampMain;
		inL++;
		inR++;
		outL++;
		outR++;
	}
}